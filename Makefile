all: jlcpcb

panel.kicad_pcb: alt-sangaboard.kicad_pcb Makefile
	kikit panelize --layout 'grid; rows: 3; cols:2; space: 4mm' --tabs annotation --cuts 'vcuts; layer: Edge.Cuts; offset: -0.1mm' --post 'millradius: 1mm'  --text 'simple; text: JLCJLCJLCJLC; vjustify: center; anchor: ml; orientation: 90deg; hoffset: 2mm;' --framing railslr --fiducials '4fid; voffset: 8mm; hoffset:3mm;' --tooling "4hole; size: 1.5mm; voffset: 3mm; hoffset: 3mm;" --copperfill 'solid; clearance: 1mm; layers: all;' alt-sangaboard.kicad_pcb panel.kicad_pcb

jlcpcb: panel.kicad_pcb alt-sangaboard.kicad_sch Makefile
	kikit fab jlcpcb panel.kicad_pcb --assembly --schematic alt-sangaboard.kicad_sch --no-drc --field JLCPCB --corrections JLC_ROT jlcpcb/
